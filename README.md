<p align="center">
    <img src="https://hasparus.now.sh/static/scissors/1.png" width="300" />
    <h1>cc-linter-config</h1>
</p>

#### Linter configs used at Chop-Chop

### Configs

## - [ESLint](./eslint/README.md)

### Versioning

Since it might not be that obvious how to version this repository:

1. Increase major version when commiting a breaking change - something that could cause eslint errors in existing code.
2. Increase minor version when doing anything that lets us do more - it won't cause new errors and will let us write code that previously could cause them.
3. Increase patch when... fixing something? Make sure it is not a breaking change though - then increase major.

Remember to change version in sub-package that you're changing and in the outer package.
