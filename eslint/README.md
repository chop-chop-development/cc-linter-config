<p align="center">
    <img src="https://hasparus.now.sh/static/scissors/1.png" width="300" />
    <h1>eslint-config-chop-chop</h1>
</p>

## Usage

### Installation

```sh
yarn add --dev eslint-config-chop-chop @typescript-eslint/eslint-plugin eslint-plugin-sonarjs eslint-plugin-react-hooks eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-import
```

### .eslintrc.js

```js
module.exports = {
    extends: ['chop-chop'], // or 'chop-chop/with-formatting-rules'
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
            },
        },
    },
    overrides: [
        {
            files: ['{*.,}{story,spec,test}.{tsx,ts,mdx}'],
            rules: {
                'import/no-default-export': 'off',
                'sonarjs/no-duplicate-string': 'off',
            },
        },
    ],
};
```

### Linting

```sh
yarn eslint src
```

## Configs

-   default -- Recommended config. Prefer Prettier over ESLint formatting rules.
-   with-formatting-rules -- Legacy config. Contains formatting rules.
