import { exec as childProcessExec } from 'child_process';
// eslint-disable-next-line import/no-extraneous-dependencies
import slash from 'slash';
import { promisify } from 'util';

const exec = promisify(childProcessExec);

const identity = <T>(x: T) => x;

const ERROR_PATH_REGEX = /(.*\): )(warning|error)/;

describe('eslint config', () => {
    it('succeeds on good file', async () => {
        const { stderr } = await exec(
            'yarn eslint -c ./eslint/index.js ./__test__/src/good/**'
        );

        expect(stderr).toBeFalsy();
    });

    it('matches snapshot on bad files', async () => {
        const { stdout } = await exec(
            'yarn eslint -f visualstudio -c ./eslint/index.js ./__test__/src/bad/**'
        ).catch(identity);

        const output = stdout
            .split('\n')
            .slice(2, -2)
            .map((s: string) => {
                const errorMessage = s.replace(ERROR_PATH_REGEX, '$2');
                const match = s.match(ERROR_PATH_REGEX);
                if (!match) {
                    return s;
                }

                const filePath = slash(match[1]);
                const slashPos = filePath.lastIndexOf('/');

                return filePath.slice(slashPos + 1) + errorMessage;
            })
            .join('\n');

        expect(output).toMatchSnapshot();
    });
});

const optionalFunction = undefined as undefined | (() => void);

optionalFunction?.();
