import { execSync } from 'child_process';

describe('eslint config', () => {
    it("doesn't conflict with prettier", () => {
        /**
         * We expect piping our `eslint/prettier` config through
         * eslint-config-prettier-check won't fail with error.
         */
        execSync('yarn prettier-check', {
            encoding: 'utf-8',
            stdio: ['ignore', 'ignore', 'pipe' /* stderr */],
        });
    });
});
