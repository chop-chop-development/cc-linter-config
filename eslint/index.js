/**
 * Use eslint-config-prettier to disable formatting rules
 */
module.exports = {
    extends: [
        './with-formatting-rules.js',
        'prettier',
        'prettier/@typescript-eslint',
        // 'prettier/babel',
        'prettier/react',
        // 'prettier/unicorn',
        // 'prettier/vue',
    ],
};
