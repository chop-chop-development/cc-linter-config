module.exports = {
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
        },
    },
    env: {
        browser: true,
        es6: true,
        jquery: true,
        jest: true,
    },
    plugins: ['sonarjs', 'react-hooks', 'simple-import-sort'],
    extends: ['airbnb', 'plugin:sonarjs/recommended'],
    parser: 'babel-eslint',
    rules: {
        'import/no-default-export': 2,
        'import/prefer-default-export': 0,
        'import/extensions': 'off',
        'import/order': 'error',
        'import/no-named-as-default': 0,
        'sonarjs/no-small-switch': 0,
        'max-len': [
            'error',
            {
                code: 100,
                ignoreUrls: true,
                ignoreTemplateLiterals: true,
                ignoreStrings: true,
            },
        ],
        camelcase: [
            2,
            {
                properties: 'never',
            },
        ],
        'comma-dangle': ['error', 'only-multiline'],
        'no-console': [
            2,
            {
                allow: ['warn', 'assert', 'error', 'info'],
            },
        ],
        'no-multi-spaces': [
            2,
            {
                ignoreEOLComments: true,
            },
        ],
        indent: 'off',
        quotes: 'off',
        'spaced-comment': 'off',
        'no-plusplus': 'off',
        'no-continue': 'off',
        'no-trailing-spaces': 'error',
        'no-underscore-dangle': 0,
        'max-classes-per-file': 'off',
        'react/jsx-fragments': 'off',
        'react/jsx-key': 'error',
        'react/jsx-indent-props': [2, 4],
        'react/jsx-indent': [2, 4],
        'react/jsx-no-bind': 'off',
        'react/jsx-max-props-per-line': 'off',
        'react/require-default-props': 'off',
        'react/no-unescaped-entities': 'off',
        'react/jsx-props-no-spreading': 'off',
        'react-hooks/exhaustive-deps': 'error',
        'react-hooks/rules-of-hooks': 'error',
        'jsx-a11y/anchor-is-valid': [
            'error',
            {
                components: ['Link'],
                specialLink: ['to'],
                aspects: ['noHref', 'invalidHref', 'preferButton'],
            },
        ],
        'jsx-a11y/label-has-for': 'off',
        'jsx-a11y/label-has-associated-control': [
            2,
            {
                labelComponents: [],
                labelAttributes: [],
                controlComponents: [],
                depth: 3,
            },
        ],
        'sonarjs/cognitive-complexity': ['error', 40],
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': [
            'warn',
            { argsIgnorePattern: '^_', varsIgnorePattern: '^_' },
        ],
    },
    overrides: [
        {
            files: ['*.ts', '*.tsx'],
            parser: '@typescript-eslint/parser',
            plugins: ['@typescript-eslint'],
            extends: [
                'plugin:@typescript-eslint/recommended',
                // 'plugin:@typescript-eslint/eslint-recommended', <- we can't extend it in config
            ],
            rules: {
                /**
                 * overrides
                 */
                'object-curly-newline': 0,
                // false positives
                'no-inner-declarations': 'off',
                'no-empty-function': 'off', // false positive on parameter constructors
                // tsx
                'react/jsx-filename-extension': 0,
                'react/prop-types': 0,
                'react/destructuring-assignment': 0,
                'react/jsx-max-props-per-line': 0,
                // TS will check this (also -- custom jsx factories)
                'react/react-in-jsx-scope': 0,
                // False positive on overloads and TS checks this
                'import/export': 0,
                // ts(2307)
                'import/no-unresolved': 0,
                /**
                 * @typescript-eslint
                 */
                '@typescript-eslint/indent': 0,
                // (bug prone) TypeScript can infer return types
                '@typescript-eslint/explicit-function-return-type': 0,
                // https://stackoverflow.com/a/54101543/6003547
                '@typescript-eslint/prefer-interface': 0,
                // Babel doesn't compile namespaces with runtime values,
                // but we can use them for nesting types,
                // API types or assigning _subcomponents_ onto a React component.
                '@typescript-eslint/no-namespace': 'off',
                // We're often builidng request bodies
                '@typescript-eslint/camelcase': 0,
                '@typescript-eslint/explicit-member-accessibility': 0,
                '@typescript-eslint/no-non-null-assertion': 0,

                // False positive on const assertions
                '@typescript-eslint/no-object-literal-type-assertion': 'off',
                '@typescript-eslint/no-empty-interface': 'off',
                '@typescript-eslint/no-parameter-properties': 'off',
                '@typescript-eslint/no-explicit-any': 'off',

                /**
                 * copied from @typescript-eslint/eslint-recommended
                 */
                /**
                 * 1. Disable things that are checked by Typescript
                 */
                // Checked by Typescript - ts(2378)
                'getter-return': 'off',
                // Checked by Typescript - ts(2300)
                'no-dupe-args': 'off',
                // Checked by Typescript - ts(1117)
                'no-dupe-keys': 'off',
                // Checked by Typescript - ts(7027)
                'no-unreachable': 'off',
                // Checked by Typescript - ts(2367)
                'valid-typeof': 'off',
                // Checked by Typescript - ts(2588)
                'no-const-assign': 'off',
                // Checked by Typescript - ts(2588)
                'no-new-symbol': 'off',
                // Checked by Typescript - ts(2376)
                'no-this-before-super': 'off',
                // This is checked by Typescript using the option `strictNullChecks`.
                'no-undef': 'off',
                // This is already checked by Typescript.
                'no-dupe-class-members': 'off',
                // This is already checked by Typescript.
                'no-redeclare': 'off',
                'no-useless-constructor': 'off',
                '@typescript-eslint/no-useless-constructor': 'error',

                // false positives
                'no-unused-expressions': 'off',
                '@typescript-eslint/no-unused-expressions': ['error'],

                // we don't need sort-imports and import/order
                // simple-import-sort/sort has an autofix
                'sort-imports': 'off',
                'import/order': 'off',
                'simple-import-sort/sort': 'error',
            },
        },
        {
            files: ['{*.,}{story,spec,test}.{tsx,ts,jsx,js}'],
            rules: {
                // storybook files aren't bundled with the app
                'import/no-extraneous-dependencies': 'off',
            },
        },
    ],
};
