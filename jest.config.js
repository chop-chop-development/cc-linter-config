module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    testPathIgnorePatterns: ['/node_modules/', '__test__/.*\\.test\\.[tj]sx?'],
};
