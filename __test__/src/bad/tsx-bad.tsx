// eslint-disable-next-line import/no-unresolved, import/no-extraneous-dependencies
import { FC, useMemo } from 'react';

// @typescript-eslint/no-unused-vars
const a = 0;

export const App = () => <div onClick={() => 2} />;

console.log('okay');

type X = {
    a: 2;
};

export const HooksBad: FC<{ foo: number }> = ({ foo }) => {
    const doubleFoo = useMemo(() => foo * 2, []);

    return <button type="button">{doubleFoo}</button>;
};
