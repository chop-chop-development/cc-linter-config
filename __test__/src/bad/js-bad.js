import { CLIEngine } from 'eslint';

export const App = () => <div onClick={() => 2} />;

console.log('okay');

export default App;

export const JsxKeyTest = () => (
    // eslint-disable-next-line react/react-in-jsx-scope
    <ul>
        {['a', 'b', 'c'].map((letter) => (
            // eslint-disable-next-line react/react-in-jsx-scope
            <li>{letter}</li>
        ))}
    </ul>
);
