/**
 * test for no-useless-constructor false positive
 */
declare class Foo {
    constructor(bar: number);

    qux: unknown[];
}

function IceCream(flavor: IceCream.Flavor) {
    return { flavor };
}

namespace IceCream {
    export enum Flavor {
        Vanilla,
        Chocolate,
    }
}

class Superhero {
    constructor(public name: string, private power: number) {}
}

const _ = new Superhero('Batman', NaN + NaN + NaN);
