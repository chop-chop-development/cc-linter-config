// eslint-disable-next-line import/no-unresolved, import/no-extraneous-dependencies
import React from 'react';

const App = () => (
    <button type="button" onClick={() => 2}>
        click me
    </button>
);

/**
 * Test if sonarjs/no-small-switch is turned off.
 */
const str = 'abc';
switch (str) {
    case 'abc':
        // eslint-disable-next-line no-console
        console.log(str);
        break;
    default:
        break;
}

export { App };

/**
 * @param {Omit<React.ComponentProps<"span">, 'role'>} props
 */
export const PomodoroEmoji = (props) => (
    <span role="img" aria-label="pomodoro" {...props}>
        🍅
    </span>
);
