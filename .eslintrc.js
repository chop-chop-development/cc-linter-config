module.exports = {
    extends: './eslint',
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
                paths: ['eslint/__test__'],
            },
        },
    },
};
